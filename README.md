DO NOT RUN ON YOUR MAIN MACHINE.
A virtual machine would be best.

This program was inspired by this post.
https://www.gnu.org/fun/jokes/evilmalware.html

This program is a slow motion fork bomb.
All the things you are not supposed to do with unix processes this does. 
It forks all the time.
It constantly changes its name.
It forks when you send it a signal.
It hides by changing its name to legit processes/services.
It writes to terminals it is not running under. 

I do not know how to kill it effectively besides shutting down the machine.
You might be able to do something clever with stopping processess but I dont know.

It is the result of too much caffine and not enough sleep.

The hackathon it was written for had a prize for most useless, this was my entry.
It lost.

It is still very useless.
It has no playload delivery ability.
It is not persistent.
It does not try to get root access.
Unless it runs for a very long time it is not very resource intensive.


To use:
make
./thingOne