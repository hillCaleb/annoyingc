#include "thingOne.h"
void resetName(){
    char name[16];
    int i;
    srandom((int)clock() + getpid());
    for(i = 0; i < 15; i++){
        name[i] = 'a' + (random() %26);
    } 
    name[15] ='\0';
    printf("%s\n", name);
    prctl(PR_SET_NAME,name,NULL,NULL,NULL);
}

void Handler(){
    fork();
    resetName();
    daemon(0,0);
    return;
}



void forkThingOne(){
    if(fork() == 0){
        printf("thing Two forking thing One\n");
        execl("thingOne","thingOne",(char*) 0);
    }else{
        exit(0);
    }
}


int main(int argc, char **argv){
    struct sigaction action;
    DIR *processDir;
    int pid;
    char buffer[100];
    action.sa_handler = Handler;
    sigemptyset(&action.sa_mask);
    sigaction(SIGINT,&action,NULL);
    sigaction(SIGHUP,&action,NULL);
    sigaction(SIGILL,&action,NULL);
    sigaction(SIGABRT,&action,NULL);
    sigaction(SIGFPE,&action,NULL);
    sigaction(SIGPIPE,&action,NULL);
    sigaction(SIGSEGV,&action,NULL);
    sigaction(SIGALRM,&action,NULL);
    sigaction(SIGUSR1,&action,NULL);
    sigaction(SIGUSR2,&action,NULL);
    sigaction(SIGTSTP,&action,NULL);
    sigaction(SIGTTIN,&action,NULL);
    sigaction(SIGTTOU,&action,NULL);
    sigaction(SIGQUIT,&action,NULL);
    sigaction(SIGTRAP,&action,NULL);
    sigaction(SIGBUS,&action,NULL);
    sigaction(SIGTERM,&action,NULL);

    
    sigaction(SIGBUS,&action,NULL);
    sigaction(SIGPOLL,&action,NULL);
    sigaction(SIGPROF,&action,NULL);
    sigaction(SIGSYS,&action,NULL);
    sigaction(SIGTRAP,&action,NULL);
    sigaction(SIGURG,&action,NULL);
    sigaction(SIGVTALRM,&action,NULL);
    sigaction(SIGXCPU,&action,NULL);
    sigaction(SIGXFSZ,&action,NULL);


    sigaction(SIGWINCH,&action,NULL);
    pid = fork();
    if(pid == 0){
        printf("Thing Two Is Running\n");
        prctl(PR_SET_NAME,"openssh",NULL,NULL,NULL);
        for(;;){
            if(argc>1){
                strcpy(buffer,"/proc/");
                strcat(buffer,argv[1]);
                processDir = opendir(buffer);
                if(!processDir){
                    closedir(processDir);
                    forkThingOne();
                }
                closedir(processDir);
            }
            sleep(1);
        }
    }else {
        resetName();        
        for(;;){
            srandom(getpid());
            if(argc>1){
                strcpy(buffer,"/proc/");
                strcat(buffer,argv[1]);
                processDir = opendir(buffer);
                if(!processDir){
                    closedir(processDir);
                    resetName();        
                    forkThingOne();
                }
                closedir(processDir);
            }
            sleep(1);
        }
    }
}


